<!-- $Id:$ -->

A First Level Header
====================

* Item 1
* Item 2
* Item 3

## Developers/Users List

| Developer   	    | Alias  	| Roles  	|
| ---------	        | -----	    | ------	|
| David Billsbrough | kc4zvw    | ITD  	    |
| aaa  	            | <nick>    | ???  	    |
| aaa  	            | <nick>  	| ???  	    |

## Developer Roles

| Roles    | Descriptions |
| -------- | ------------ |
| Planning (P) | People interested in planning and coordination |
| Instructor (I) | People interested in participating as instructors creating courses |
| Technical (T) | People interested in helping in the technical infrastructure |
| Designers (D) | People interested in Moodle customization, interface appearance, graphics/web design and UX |
| Other (O) | People interested in helping in being subject matter experts on various tools and processes |

## sample table

|   	|   	|   	|   	|   	|
|---	|---	|---	|---	|---	|
|   	|   	|   	|   	|   	|
|   	|   	|   	|   	|   	|
|   	|   	|   	|   	|   	|


----
Revised: Sunday, August 28, 2022 at 10:15:19 AM (EDT)
