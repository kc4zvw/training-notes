# Topics

  What's the deal here?

* link to wiki: https://salsa.debian.org/kc4zvw/training-notes/-/wikis/home

## Aims:

1. Learning topics/tasks
2. Setup up a personel moodle install? (when)
3. ???
4. What is a good markdown editor?
5. Will LibreOffice be useful?


## Whoami

* Email: [David](mailto:dbillsbrough@gmail.com) (david.billsbrough AT gmx DOT com)
* IRC: kc4zvw (Libre.chat)
* Location: 28.641N / -81.123W
* GPG key: 
* Blogspot: [KC4ZVW](http://kc4zvw.blogspot.com/) Amateur Radio operator
* QSL.net: [KC4ZVW](https://www.qsl.net/kc4zvw)
* Wordpress blog: [KC4ZVW](https://kc4zvw.wordpress.org) radio stuff
* Web Site: [KC4ZVW](https://www.kc4zvw.org)

----
David (kc4zvw)
