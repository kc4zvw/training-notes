#Role Titles and Descriptions#

What are the various develper roles in the Debian Academy?

** Proposal One **

| Roles    | Descriptions |
| -------- | ------------ |
| Planning (P) | People interested in planning and coordination |
| Instructor (I) | People interested in participating as instructors creating courses |
| Technical (T) | People interested in helping in the technical infrastructure |
| Designers (D) | People interested in Moodle customization, interface appearance, graphics/web design and UX |
| Other (O) | People interested in helping in being subject matter experts on various tools and processes |


## Planning (P)

  These people support the team by figuring out what classes are to be created, outlining the topics are covered and finding people to bring the classes into existance.

## Instructor (I)

  These people support maintain all course work in Moodle like enroll students, grading quizes/tests and ensuring class scheduling and course completion.  They are Teachers!

## Technical (T)

  These people support the infrastructure both in the common Debian Projects and Debian Academy Team Resources.

## Designers (D)

  These people can create various media like: Moodle badges, web artwork, tables and charts.

## Other (O)

  Subject matter experts for various tools latex, H5p content, articles, quizes, workbooks, howtos, and audio/video tutorials.


## Notes:

  Build this idea/implementation out more fully

----
Revised: Sunday, August 28, 2022 at 10:26:39 AM (EDT)
